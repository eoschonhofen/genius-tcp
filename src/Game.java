import java.util.ArrayList;
import java.util.List;

public class Game {
    private static final int STATE_WAITING = 0;
    private static final int STATE_PRESS_BUTTON = 1;
    public static final int STATE_SEE_BUTTON = 2;
    private static final int STATE_LOST = 3;
    private static final int START_SCORE = 0;
    private static final int NORMAL_GAME=4;
    private static GameSequence Sequencia=new GameSequence();
    private static int state;
    private static  int score=0;
    private double blinkSpeed;
    private static List<Integer> IdButtonSequence=new ArrayList<Integer>();
    private final double INCREMENT_SPEED = 1.0;
    private Game SavedGame;
    private ButtonsManager botoes = new ButtonsManager();
    static String playerName = "Player";
    public static final int RED = 0;
    public static final int GREEN = 1;
    public static final int BLUE = 2;
    public static final int YELLOW = 3;


    public static int getState() {
        return state;


    }

    public void setState(int state) {
        this.state = state;
    }



    public static int getScore() {
        return score;
    }


    public static void incScore() {
        score++;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void setSequence(GameSequence Sequence) {
        //this.sequence = Sequence;
    }


    public static void GameCallButtonPressed(int i)
    {
        AudioManager.playSound(i);
        if(InPressButtonState()) {
            if (pressedWrongButton(i)) {
                setGameStateToLost();
                EndMenu.showMenu();
                HighScores.LoadScore();
                if(HighScores.scoreBeaten(score))
                {
                    EndMenu.getName();
                }
            }

            Sequencia.incActualState();


            if (Sequencia.hasFinished()) {
                setGameStateToSeeButtons();
                Sequencia.ClearActualState();
                incScore();
                Sequencia.insertSequence(Sequencia.GenerateRandomButton(NORMAL_GAME));

                //GUIGame.ShowGame();
            }

        }
        if(InSeeButtonState()) {
            Sequencia.ShowSequence();
            GUIGame.UpdateSequenceOfColor(Sequencia.ListSequence());
            setGameStateToPressButton();

        }
        }

    private static boolean pressedWrongButton(int i) {
        return !Sequencia.checkNextElement(i);
    }

    private static boolean InSeeButtonState() {
        return state==STATE_SEE_BUTTON;
    }

    private static boolean InPressButtonState() {
        return state==STATE_PRESS_BUTTON;
    }

    private static void setGameStateToLost() {
        state = STATE_LOST;
    }

    private static void setGameStateToPressButton() {
        state=STATE_PRESS_BUTTON;
    }


    public static void Play()
    {
        setGameStateToSeeButtons();
        score=START_SCORE;
        Sequencia.StartSequence();
        Sequencia.insertSequence(Sequencia.GenerateRandomButton(NORMAL_GAME));
        Sequencia.ShowSequence();
        GUIGame.UpdateSequenceOfColor(Sequencia.getIdButtonSequence());
        setGameStateToPressButton();

    }

    private static void setGameStateToSeeButtons() {
        state=STATE_SEE_BUTTON;
    }

}


