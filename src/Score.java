public class Score implements Comparable<Score> {
    String Nome;
    int Score;

    Score(String Nome, int Score) {
        this.Nome = Nome;
        this.Score = Score;
    }
    @Override
    public int compareTo(Score otherScore)
    {
        if(this.Score < otherScore.Score)
            return 1;
        else if(this.Score>otherScore.Score)
            return -1;
        else
            return 0;

    }


}