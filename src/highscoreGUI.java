import javax.swing.*;
import java.awt.*;

public class highscoreGUI {
    public static void showHighScore() {
        JFrame frame = new JFrame();


        Object rowData[][] = {{"Row1-Column1", "Row1-Column2"},
                {"Row2-Column1", "Row2-Column2"},{"Row1-Column1", "Row1-Column2"},{"Row1-Column1", "Row1-Column2"},{"Row1-Column1", "Row1-Column2"},{"Row1-Column1", "Row1-Column2"},{"Row1-Column1", "Row1-Column2"},{"Row1-Column1", "Row1-Column2"},{"Row1-Column1", "Row1-Column2"},{"Row1-Column1", "Row1-Column2"}};
        Object columnNames[] = {"Nome", "Score",};
        JTable table = new JTable(rowData, columnNames);

        JScrollPane scrollPane = new JScrollPane(table);
        frame.add(scrollPane, BorderLayout.CENTER);
        frame.setSize(300, 150);
        frame.setVisible(true);
        for (int i = 0; i < HighScores.MAX_SCORES; i++) {
            table.setValueAt(HighScores.GetScores().get(i).Nome, i, 0);
            table.setValueAt(HighScores.GetScores().get(i).Score, i, 1);
        }
    }
}
