import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class GUIGame {
    private  JPanel panel2;
    public  JButton RedButton;
    public  JButton YellowButton;
    public  JButton BlueButton;
    public  JButton GreenButton;
    private  JButton playButton;
    private  JButton muteButton;
    private  static GUIGame game;
    public final static Color SHADOWRED =  new Color(125,0,0);
    public final static Color SHADOWBLUE =  new Color(0,0,125);
    public final static Color SHADOWYELLOW =  new Color(125,125,0);
    public final static Color SHADOWGREEN=  new Color(0,125,0);

    public final static Color NORMALRED =  new Color(255,0,0);
    public final static Color NORMALBLUE =  new Color(0,0,255);
    public final static Color NORMALYELLOW =  new Color(255,255,0);
    public final static Color NORMALGREEN =  new Color(0,255,0);



    static Timer TimerToResetColor;
    static Timer TimerToBlinkButtons;



    private static JFrame frame;

    public GUIGame() {



        ActionListener ResetColors = new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                YellowButton.setBackground(NORMALYELLOW);
                BlueButton.setBackground(NORMALBLUE);
                RedButton.setBackground(NORMALRED);
                GreenButton.setBackground(NORMALGREEN);
            }
        };
        ActionListener BlinkButton = new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                game.blinkButton(Sequencia.get(index));
                index++;
                if(EndOfSequence())
                    TimerToBlinkButtons.stop();
                else
                    TimerToBlinkButtons.start();
            }

            private boolean EndOfSequence() {
                return (Sequencia.size()==index);
            }
        };
        int timerDelay = 2 * 1000; // or whatever length of time needed
        TimerToResetColor = new Timer(timerDelay, ResetColors);
        TimerToBlinkButtons =new Timer(timerDelay,BlinkButton);

// later on in the block where you want to start your Timer
        TimerToResetColor.start();


        playButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Game.Play();

            }
        });
        muteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(AudioManager.isMuted())
                    AudioManager.unmute();
                else
                    AudioManager.mute();


            }
        });

        RedButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                Game.GameCallButtonPressed(Game.RED);

            }
        });

        BlueButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Game.GameCallButtonPressed(Game.BLUE);

            }
        });
        GreenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Game.GameCallButtonPressed(Game.GREEN);

            }
        });

        YellowButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Game.GameCallButtonPressed(Game.YELLOW);

            }
        });
    }





    public static void ShowGame(){
        game=new GUIGame();
        frame = new JFrame("GENIUS");
        frame.setContentPane(game.panel2);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }


    public void Updategame(){

        frame.setContentPane(game.panel2);
        frame.pack();
        frame.setVisible(true);
    }


    static List<Integer>Sequencia;
    static int index;



    public static void UpdateSequenceOfColor(List<Integer> SequenciaID) {

        Sequencia=SequenciaID;
        index=0;
        TimerToBlinkButtons.start();
    }




    public  void blinkButton( int decideBotao) {
        AudioManager.playSound(decideBotao);

        if (decideBotao == Game.RED) {

            RedButton.setBackground(SHADOWRED);


        }
        else if (decideBotao == Game.BLUE) {

            BlueButton.setBackground(SHADOWBLUE);
        }
        else if (decideBotao == Game.YELLOW){

            YellowButton.setBackground(SHADOWYELLOW);
        }
        else if(decideBotao == Game.GREEN){
            GreenButton.setBackground(SHADOWGREEN);
        }
        TimerToResetColor.start();
    }

}
