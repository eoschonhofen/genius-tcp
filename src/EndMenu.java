import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class EndMenu {
    private JButton playButton;
    private JPanel panel1;
    private JButton highscoreButton;
    private JButton exitButton;

    public EndMenu() {
        playButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GUIGame.ShowGame();
            }
        });

        highscoreButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                HighScores.LoadScore();

                highscoreGUI.showHighScore();
            }
        });
        exitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

    }

    public static void showMenu() {
        JFrame frame = new JFrame("Fim de Jogo");
        frame.setContentPane(new EndMenu().panel1);
        frame.pack();
        frame.setVisible(true);
    }

    public static void getName() {
        JLabel label = new JLabel("Insira seu nome");
        JTextField inputName = new JTextField(Game.playerName, 20);

        JPanel panel = new JPanel();

        panel.add(label);
        panel.add(inputName);
        inputName.requestFocus();

        JButton botao = new JButton("Salvar");
        panel.add(botao);

        JFrame frame = new JFrame("Novo Highscore");
        frame.add(panel);
        frame.setSize(250, 125);
        frame.setVisible(true);

        botao.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent event) {

                Game.playerName = inputName.getText();
                HighScores.LoadScore();
                HighScores.insertScore(Game.getScore());
                HighScores.OrdenaScores();
                HighScores.SaveScore();

                frame.setVisible(false);
                frame.setEnabled(false);
                frame.pack();
            }
        });
    }
}
