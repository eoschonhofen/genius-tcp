import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Configurations {

    public void start() {
        JLabel label[] = new JLabel[4];
        label[0] = new JLabel("Som do Botão Vermelho");
        label[1] = new JLabel("Som do Botão Verde");
        label[2] = new JLabel("Som do Botão Azul");
        label[3] = new JLabel("Som do Botão Amarelo");

        JTextField textField[] = new JTextField[4];

        for(int i=0; i <= 3; i++) {
            textField[i] = new JTextField(AudioManager.soundPath[i], 20);
        }

        JPanel panel = new JPanel();


        for(int i=0; i <= 3; i++) {
            panel.add(label[i]);
            panel.add(textField[i]);
        }

        JButton botao = new JButton("Salvar");
        panel.add(botao);

        botao.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent event) {

                for (int i = 0; i <= 3; i++) {
                    AudioManager.soundPath[i] = textField[i].getText();
                }

                for(int i=0; i <= 3; i++) {
                    System.out.println(AudioManager.soundPath[i]);
                }

                AudioManager.loadSounds();
            }
        });

        JFrame frame = new JFrame();
        frame.add(panel);
        frame.setSize(250, 260);
        frame.setVisible(true);

        for(int i=0; i <= 3; i++) {
            textField[i].requestFocus();
        }
    }
}