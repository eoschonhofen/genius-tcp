import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Menu {
    private JButton playButton;
    private JPanel panel1;
    private JButton configButton;
    private JButton highScoresButton;
    private JButton exitButton;

    public  Menu() {
        playButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GUIGame.ShowGame();
            }
        });

        configButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Configurations Config=new Configurations();
              Config.start();
           }
        });


        highScoresButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                HighScores.LoadScore();
                highscoreGUI.showHighScore();
            }
        });
        exitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

    }
        public static void showMenu() {
            JFrame frame = new JFrame("GENIUS");
            frame.setContentPane(new Menu().panel1);
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.pack();
            frame.setVisible(true);
        }



}