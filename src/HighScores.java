import java.io.*;
import java.nio.file.Files;
import java.util.*;

public class HighScores {


    private static List<Score> Scores=new ArrayList<Score>();
    private static final String HighScoreFile = "scores.txt";
    public static final int  MAX_SCORES=10;

    public static void LoadScore() {
        try {
            File ScoreFile = new File(HighScoreFile);
            Scanner scoreParser = new Scanner(ScoreFile);
            while (scoreParser.hasNextLine()) {
                String Linha = scoreParser.nextLine();
                System.out.println(Linha);
                String[] line = Linha.split(" ");
                List<String> tuple = Arrays.asList(line);

                Score readScore = new Score(tuple.get(0), Integer.parseInt(tuple.get(1)));

                Scores.add(readScore);

            }
        }
        catch(Exception ex)
        {
            for(int i=0;i<MAX_SCORES;i++) {
                Score newScore = new Score("AAA", 0);
                Scores.add(newScore);
            }
        }
        }
        public static void SaveScore()
        {
            try {
            BufferedWriter ScoreFile = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(HighScoreFile), "utf-8"));
                for(int i=0;i<MAX_SCORES;i++) {
                    ScoreFile.write(new String(Scores.get(i).Nome + " " + Scores.get(i).Score));
                    ScoreFile.newLine();
                    ScoreFile.flush();
            }

            }
            catch(Exception ex)
            {


            }


        }
        public static boolean scoreBeaten(int score)
        {

            for(int i=0;i<Scores.size();i++)
            {

                if(Scores.get(i).Score<score)
                    return true;

            }
            return false;

        }
        public static void insertScore(int score)
        {
            Scores.add(new Score(Game.playerName,score));
        }
        public static List<Score> GetScores()
        {
            return new ArrayList<Score>(Scores);
        }

        public static void OrdenaScores()
        {
            Collections.sort(Scores);
        }
        }



