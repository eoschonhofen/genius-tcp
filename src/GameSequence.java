import java.sql.Time;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GameSequence
{
    private final int START_STATE=0;
    private List<Integer> IdButtonSequence;
    private int actualState;

    public List<Integer> getIdButtonSequence()
    {
        return IdButtonSequence;
    }
    public void insertSequence(int idButton)
    {
        IdButtonSequence.add(idButton);
    }
    public int GenerateRandomButton(int maximum)
    {
        Random rand = new Random();
        rand.setSeed(System.currentTimeMillis());
        return rand.nextInt(maximum);
    }
    public boolean checkNextElement(int atualElement)
    {
        System.out.println(this.actualState);
        return IdButtonSequence.get(actualState)==atualElement;
    }
    public void StartSequence()
    {
        actualState=START_STATE;
        IdButtonSequence = new ArrayList<Integer>();
    }

    public boolean hasFinished()
    {
        return this.actualState==this.IdButtonSequence.size();

    }
    public void ClearActualState()
    {
        this.actualState=START_STATE;
    }
    public void incActualState()
    {
        this.actualState++;
    }
    public void ShowSequence()
    {
        System.out.println(IdButtonSequence.toString());

    }
    public List<Integer> ListSequence()
    {
        return new ArrayList<Integer>(IdButtonSequence);

    }

}
