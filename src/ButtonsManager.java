import java.util.ArrayList;
import java.util.List;

public class ButtonsManager {
    private List<GameButton> Botoes=new ArrayList<GameButton>();
    private static boolean pressed=false;
    private static int buttonPressed;

    public static boolean NotPressed()
    {
        return !pressed;

    }
    public static void Press()
    {
        pressed=true;

    }
    public static void Unpress()
    {
        pressed=false;

    }
    public static int ValorLido()
    {
        return buttonPressed;
    }

    public static void LerValor(int press)
    {
        buttonPressed=press;
    }


    public void InsertButton(GameButton Button)
    {
        Botoes.add(Button);
    }
    public ArrayList<GameButton> GetBotoes()
    {
        return new ArrayList<GameButton>(Botoes);

    }
    public void pressButton(int buttonId)
    {
        Botoes.get(buttonId).Press();
    }

}
