import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;
import java.io.File;

public class AudioManager {

	private static final Boolean MUTE_VOLUME = true;
	private static final Boolean UNMUTE_VOLUME = false;
	private static Boolean muted = UNMUTE_VOLUME;

	public static void mute() {
		muted = MUTE_VOLUME;
	}

	public static void unmute() {
		muted = UNMUTE_VOLUME;
	}

	public static Boolean isMuted() {
		return muted;
	}

	static AudioInputStream sound[] = new AudioInputStream[4];
	static String soundPath[] = {"do.wav", "mi.wav", "sol.wav", "si.wav"};

	public static void loadSounds() {
		try {
			for(int i=0; i <= 3; i++) {
				sound[i] = AudioSystem.getAudioInputStream(new File(soundPath[i]).getAbsoluteFile());
				}
	} catch (Exception ex) {
				System.out.println("Erro ao abrir os arquivos de sons.");
				ex.printStackTrace();
				}
	}

	public static void playSound(int sel) {
		try {

			loadSounds(); //Única maneira que encontramos para tocar o mesmo áudio mais de uma vez.

			Clip clip = AudioSystem.getClip();

			clip.open(sound[sel]);
			clip.stop();
			clip.setFramePosition(0);



			FloatControl volume = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);

			if (AudioManager.isMuted())
				volume.setValue(-80.0f);

			if (!AudioManager.isMuted())
				volume.setValue(0.0f);

			clip.start();



		} catch (Exception ex) {
			System.out.println("Erro ao tocar som.");
			ex.printStackTrace();
		}
	}
}